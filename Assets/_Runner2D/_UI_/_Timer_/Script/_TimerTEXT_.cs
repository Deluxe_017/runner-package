using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class _TimerTEXT_ : MonoBehaviour
{
    public Text ScoreTxt;
    private float ScoreValue = 0f;
    private float IncreasedPerSecond = 1f;

    public _LoadWorld_ objectManager;

    private void FixedUpdate()
    {
        ScoreTxt.text = ((int)ScoreValue).ToString();

        ScoreValue += IncreasedPerSecond * Time.deltaTime;

        objectManager.CheckAndSwapObjects(ScoreValue);
    }
}