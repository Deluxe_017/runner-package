using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class _LoadWorld_ : MonoBehaviour
{
    public GameObject[] DisableObject1;
    public GameObject[] ActivateObject1;

    public GameObject[] DisableObject2;
    public GameObject[] ActivateObject2;

    public GameObject[] DisableObject3;
    public GameObject[] ActivateObject3;

    private bool _objectsSwapped1 = false;
    private bool _objectsSwapped2 = false;
    private bool _objectsSwapped3 = false;

    public void CheckAndSwapObjects(float scoreValue)
    {
        if (scoreValue >= 10 && !_objectsSwapped1)
        {
            SwapObjects(DisableObject1, ActivateObject1);
            _objectsSwapped1 = true;
        }

        if (scoreValue >= 20 && !_objectsSwapped2)
        {
            SwapObjects(DisableObject2, ActivateObject2);
            _objectsSwapped2 = true;
        }

        if (scoreValue >= 30 && !_objectsSwapped3)
        {
            SwapObjects(DisableObject3, ActivateObject3);
            _objectsSwapped3 = true;
        }
    }

    void SwapObjects(GameObject[] objectsToDeactivate, GameObject[] objectsToActivate)
    {
        foreach (GameObject obj in objectsToDeactivate)
        {
            obj.SetActive(false);
        }

        foreach (GameObject obj in objectsToActivate)
        {
            obj.SetActive(true);
        }
    }
}