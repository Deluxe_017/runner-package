using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UIElements;

public class PlayerDeadController : MonoBehaviour
{
     public Animator PlayerAnimator;

     private void OnCollisionEnter2D(Collision2D collision)
     {
         if (collision.gameObject.CompareTag("Obstacule"))
         {
             ActivateDeathAnimation();
         }
     }

     void ActivateDeathAnimation()
     {
         PlayerAnimator.SetTrigger("AmyDead");
     }
}