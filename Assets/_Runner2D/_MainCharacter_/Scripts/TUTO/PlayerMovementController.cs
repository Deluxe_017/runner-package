using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementController : MonoBehaviour
{
    private Rigidbody2D Rb;
    private Animator Animator;
    private LayerMask GroundLayer;
    private Transform FeetPos;

    protected float JumpForce = 5.5f;
    protected float GroundDistance = 0.3f;
    protected float JumpTime = 0.4f;

    protected bool _isGrounded = false;
    protected bool _isJumping = false;
    protected float _jumpTimer;

    private void Awake()
    {
        Rb = GetComponent<Rigidbody2D>();
        Animator = GetComponent<Animator>();

        GroundLayer = LayerMask.GetMask("Ground");
        FeetPos = transform.Find("Feet");
    }

    private void Update()
    {
        _isGrounded = Physics2D.OverlapCircle(FeetPos.position, GroundDistance, GroundLayer);

        if (_isGrounded && Input.GetButtonDown("Jump"))
        {
             _isJumping = true;
             Rb.velocity = Vector2.up * JumpForce;
             Animator.SetBool("IsJumping", true);
        }

        if (Input.GetButtonUp("Jump"))
        {
             _isJumping = false;
             _jumpTimer = 0;
             Animator.SetBool("IsJumping", false);
        }
    }
}