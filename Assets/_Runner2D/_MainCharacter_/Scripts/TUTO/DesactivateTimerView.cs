using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DesactivateTimerView : MonoBehaviour
{
    public GameObject TimerView;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Obstacule"))
        {
            StartCoroutine(PostCollisionSequence());
        }
    }

    IEnumerator PostCollisionSequence()
    {
        yield return new WaitForSeconds(1);

        PauseGame();
        Panelscore();
    }

    void PauseGame()
    {
        Time.timeScale = 0;
    }

    void Panelscore()
    {
        TimerView.SetActive(false);
    }
}