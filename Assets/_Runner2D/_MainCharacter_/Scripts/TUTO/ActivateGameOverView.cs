using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateGameOverView : MonoBehaviour
{
    public GameObject GameOverView;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Obstacule"))
        {
            StartCoroutine(PostCollisionSequence());
        }
    }

    IEnumerator PostCollisionSequence()
    {
        yield return new WaitForSeconds(1);

        PanelDeadPlayer();
        PauseGame();
    }

    void PanelDeadPlayer()
    {
        GameOverView.SetActive(true);
    }

    void PauseGame()
    {
        Time.timeScale = 0;
    }
}