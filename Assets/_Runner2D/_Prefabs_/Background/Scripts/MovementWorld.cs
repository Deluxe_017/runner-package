using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementWorld : MonoBehaviour
{
    protected float Speed = 8f;

    private void Update()
    {   
        transform.Translate(Vector2.left * Speed * Time.deltaTime);
    }

    public void StopMovementOfObjects()
    {
        Speed = 0f;
    }
}