using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundRespawn : MonoBehaviour
{
    private float _resetPosition = -40;
    private float _spawnPosition = 48.5f;

    private bool _isMoving = true;

    void Update()
    {
        if (_isMoving)
        {
            if (transform.position.x <= _resetPosition)
            {
                RepositionBackground();
            }
        }
    }

    void RepositionBackground()
    {
        transform.position = new Vector2(_spawnPosition, transform.position.y);
    }

    public void StopBackgroundMovement()
    {
        _isMoving = false;
    }
}