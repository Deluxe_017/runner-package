using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopMovementWorld : MonoBehaviour
{
    public GameObject[] MovableObjects;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Obstacule"))
        {
            StopMovementOfObjects();
        }
    }

    void StopMovementOfObjects()
    {
        foreach (GameObject obj in MovableObjects)
        {
            var movementScript = obj.GetComponent<MovementWorld>();
            if (movementScript != null)
            {
                movementScript.StopMovementOfObjects();
            }
        }
    }
}